from __future__ import print_function

import datetime
import csv

from airflow import models
from airflow.operators import bash_operator
from airflow.operators import python_operator
from airflow.contrib.operators import bigquery_operator
from airflow.contrib.operators import gcs_to_bq
from airflow.contrib.operators.gcs_to_bq import GoogleCloudStorageToBigQueryOperator

#bq_dest_table = 'my-project-1495739920026:aramarkUAT.airflow_farms_raw'

default_dag_args = {
    # The start_date describes when a DAG is valid / can be run. Set this to a
    # fixed point in time rather than dynamically, since it is evaluated every
    # time a DAG is parsed. See:
    # https://airflow.apache.org/faq.html#what-s-the-deal-with-start-date
    'start_date': datetime.datetime(2018, 1, 1),
    'owner': 'farmlogix',
    'depends_on_past': False,
    'email': ['fred.bliss@aptitive.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'schedule_interval': '@monthly',    
}

# Define a DAG (directed acyclic graph) of tasks.
# Any task you create within the context manager is automatically added to the
# DAG object.
with models.DAG(
        'bqtest-raw',
        default_args=default_dag_args) as dag:

#Load Raw Layer - business_type
    bq_raw_load_gcs_business_type = gcs_to_bq.GoogleCloudStorageToBigQueryOperator(
    task_id='bq_raw_load_gcs_business_type',
    bucket='aramark-datalake',
    destination_project_dataset_table='my-project-1495739920026:FarmLogixRaw.business_type',
    source_objects=[
            'business_type.csv'
        ],
    schema_fields= {'fields':
      [{'name': 'pk', type: 'INTEGER'},
      {'name': 'created_by', type: 'INTEGER'},
      {'name': 'active_dt', type: 'DATETIME'},
      {'name': 'inactive_dt',type:'DATETIME'},
      {'name': 'name', type: 'STRING'},
      {'name': 'organization_pk', type:'INTEGER'},
      {'name': 'import_id', type: 'INTEGER'}
      ]},
      source_format='csv',
      skip_leading_rows=0,
      allow_jagged_rows='true',
      allow_quoted_newlines='true',
      write_disposition='WRITE_TRUNCATE')
      
    #Load Raw Layer - customers

    bq_raw_load_gcs_customers = gcs_to_bq.GoogleCloudStorageToBigQueryOperator(
    task_id='bq_raw_load_gcs_customers',
    bucket='aramark-datalake',
    destination_project_dataset_table='my-project-1495739920026:FarmLogixRaw.customers',
    source_objects=[
            'customers.csv'
        ],
    schema_fields= {'fields':
        [{'name': 'pk', type: 'INTEGER'},
        {'name': 'name', type: 'STRING'},
        {'name': 'parent_pk', type: 'INTEGER'},
        {'name': 'customer_type',type:'INTEGER'},
        {'name': 'order_guide_pk', type: 'INTEGER'},
        {'name': 'active_dt', type:'DATETIME'},
	    {'name': 'inactive_dt', type:'DATETIME'},
	    {'name': 'use_parent_image', type:'INTEGER'},
	    {'name': 'is_test', type:'INTEGER'},
    	{'name': 'ship_name',type:'STRING'},
	    {'name': 'organization_pk', type:'INTEGER'},
	    {'name': 'shipping_info_contact1', type:'STRING'},
	    {'name': 'shipping_info_contact2', type:'STRING'},
	    {'name': 'primary_address_pk', type:'INTEGER'},
	    {'name': 'tracking_guide_pk', type:'INTEGER'},
	    {'name': 'created_by', type:'INTEGER'},
	    {'name': 'logo_id', type:'INTEGER'},
	    {'name': 'invoicing_guide_pk', type:'INTEGER'},
	    {'name': 'pre_order_guide_pk', type:'INTEGER'},
	    {'name': 'import_id', type:'INTEGER'}
  ]},
      source_format='csv',
      skip_leading_rows=0,
      allow_jagged_rows='true',
      allow_quoted_newlines='true',
      write_disposition='WRITE_TRUNCATE')

#Load Raw Layer - distributor_addresses
    bq_raw_load_gcs_distributor_addresses = gcs_to_bq.GoogleCloudStorageToBigQueryOperator(
    task_id='bq_raw_load_gcs_distributor_addresses',
    bucket='aramark-datalake',
    destination_project_dataset_table='my-project-1495739920026:FarmLogixRaw.distributor_addresses',
    source_objects=[
            'distributor_addresses.csv'
        ],
    schema_fields= {'fields':
      [{'name': 'active_dt', type:'DATETIME'},
	    {'name': 'address_type', type:'INTEGER'},
	    {'name': 'city', type:'STRING'},
	    {'name': 'distributor_pk', type:'INTEGER'},
	    {'name': 'inactive_pk',type:'INTEGER'},
	    {'name': 'pk', type:'INTEGER'},
	    {'name': 'state', type:'STRING'},
	    {'name': 'street_one', type:'STRING'},
	    {'name': 'street_two', type:'STRING'},
	    {'name': 'zip', type:'STRING'},
	    {'name': 'latitude', type:'STRING'},
	    {'name': 'longitude', type:'STRING'},
	    {'name': 'geocoding_note', type:'STRING'},
	    {'name': 'geocoded_address', type:'STRING'},
	    {'name': 'import_id', type:'INTEGER'}
        ]},
      source_format='csv',
      skip_leading_rows=0,
      allow_jagged_rows='true',
      allow_quoted_newlines='true',
      write_disposition='WRITE_TRUNCATE')

#Raw Layer Load- distributors
    bq_raw_load_gcs_distributors = gcs_to_bq.GoogleCloudStorageToBigQueryOperator(
    task_id='bq_raw_load_gcs_distributors',
    bucket='aramark-datalake',
    destination_project_dataset_table='my-project-1495739920026:FarmLogixRaw.distributors',
    source_objects=[
            'distributors.csv'
        ],
    schema_fields= {'fields':
      [{'name': 'pk', type:'INTEGER'},
	    {'name': 'name', type:'STRING'},
	    {'name': 'code', type:'STRING'},
	    {'name': 'latitude', type:'STRING'},
	    {'name': 'longitude', type:'STRING'},
	    {'name': 'active_dt', type:'DATETIME'},
	    {'name': 'inactive_dt', type:'DATETIME'},
	    {'name': 'request_dt_interval', type:'INTEGER'},
	    {'name': 'greeting_message', type:'STRING'},
	    {'name': 'is_test', type:'INTEGER'},
	    {'name': 'organization_pk', type:'INTEGER'},
	    {'name': 'long_name', type:'STRING'},
	    {'name': 'primary_address_pk', type:'INTEGER'},
	    {'name': 'billing_info_contact1', type:'STRING'},
        {'name': 'billing_info_contact2', type:'STRING'},
        {'name': 'created_by', type:'INTEGER'},
        {'name': 'logo_id', type:'INTEGER'},
        {'name': 'parent_pk', type:'INTEGER'},
        {'name': 'parent_pk', type:'INTEGER'}
        ]},
      source_format='csv',
      skip_leading_rows=0,
      allow_jagged_rows='true',
      allow_quoted_newlines='true',
      write_disposition='WRITE_TRUNCATE')

#Load Raw Layer- farm_addresses
    bq_raw_load_gcs_farm_addresses = gcs_to_bq.GoogleCloudStorageToBigQueryOperator(
    task_id='bq_raw_load_gcs_farm_addresses',
    bucket='aramark-datalake',
    destination_project_dataset_table='my-project-1495739920026:FarmLogixRaw.farm_addresses',
    source_objects=[
            'farm_addresses.csv'
        ],
    schema_fields= {'fields':
        [{'name': 'active_dt', type:'DATETIME'},
        {'name': 'address_type', type:'INTEGER'},
        {'name': 'city', type:'STRING'},
        {'name': 'farm_pk', type:'INTEGER'},
        {'name': 'inactive_dt', type:'DATETIME'},
        {'name': 'pk', type:'INTEGER'},
        {'name': 'state', type:'STRING'},
        {'name': 'street1', type:'STRING'},
        {'name': 'street2', type:'STRING'},
        {'name': 'zip', type:'STRING'},
	    {'name': 'latitude', type:'STRING'},
	    {'name': 'longitude', type:'STRING'},
	    {'name': 'geocoding_note', type:'STRING'},
	    {'name': 'geocoded_address', type:'STRING'},
	    {'name': 'import_id', type:'INTEGER'}
        ]},
      source_format='csv',
      skip_leading_rows=0,
      allow_jagged_rows='true',
      allow_quoted_newlines='true',
      write_disposition='WRITE_TRUNCATE')

#farms
    bq_raw_load_gcs_farms = gcs_to_bq.GoogleCloudStorageToBigQueryOperator(
    task_id='bq_raw_load_gcs_farms',
    bucket='aramark-datalake',
    destination_project_dataset_table='my-project-1495739920026:FarmLogixRaw.farms',
    source_objects=[
            'farms.csv'
        ],
    schema_fields= {'fields':
      [{'name': 'pk', type:'INTEGER'},
        {'name': 'name', type:'STRING'},
        {'name': 'code', type:'STRING'},
	    {'name': 'url', type:'STRING'},
        {'name': 'is_test', type:'INTEGER'},
        {'name': 'active_dt', type:'DATETIME'},
        {'name': 'inactive_dt', type:'DATETIME'},
        {'name': 'organization_pk', type:'INTEGER'},
        {'name': 'primary_address_pk', type:'INTEGER'},
        {'name': 'created_by', type:'INTEGER'},
        {'name': 'lead_interval', type:'INTEGER'},
        {'name': 'products_visible', type:'BOOLEAN'},
	    {'name': 'parent_pk', type:'INTEGER'},
	    {'name': 'logo_id', type:'INTEGER'},
	    {'name': 'use_parent_image', type:'BOOLEAN'},
	    {'name': 'farm_hub', type:'BOOLEAN'},
        {'name': 'hide_product_if_certificate_expired', type:'BOOLEAN'},
        {'name': 'import_id', type:'INTEGER'}
        ]},
      source_format='csv',
      skip_leading_rows=0,
      allow_jagged_rows='true',
      allow_quoted_newlines='true',
      write_disposition='WRITE_TRUNCATE')


#order_items
    bq_raw_load_gcs_order_items = gcs_to_bq.GoogleCloudStorageToBigQueryOperator(
    task_id='bq_raw_load_gcs_order_items',
    bucket='aramark-datalake',
    destination_project_dataset_table='my-project-1495739920026:FarmLogixRaw.order_items',
    source_objects=[
            'order_items.csv'
        ],
    schema_fields= {'fields':
      [{'name': 'pk', type:'INTEGER'},
        {'name': 'order_pk', type:'INTEGER'},
        {'name': 'sku_pk', type:'INTEGER'},
        {'name': 'quantity', type:'FLOAT'},
	    {'name': 'base_price', type:'FLOAT'},
        {'name': 'sell_price', type:'FLOAT'},
        {'name': 'resell_price', type:'FLOAT'},
        {'name': 'create_dt', type:'DATETIME'},
        {'name': 'update_dt', type:'DATETIME'},
        {'name': 'lead_interval', type:'INTEGER'},
        {'name': 'harvest_date', type:'DATE'},
        {'name': 'packing_date', type:'DATE'},
        {'name': 'shipping_date', type:'DATE'},
	    {'name': 'delivery_type', type:'STRING'},
	    {'name': 'distributor_pk', type:'INTEGER'},
	    {'name': 'purchase_order_id', type:'INTEGER'},
	    {'name': 'delivery_date', type:'DATE'},
        {'name': 'invoice_customer_pk', type:'INTEGER'},
        {'name': 'customer_city', type:'STRING'},
        {'name': 'customer_state', type:'STRING'},
        {'name': 'class', type:'STRING'},
        {'name': 'cp_invoice_nbr', type:'STRING'},
        {'name': 'vendor_invoice_nbr', type:'STRING'},
        {'name': 'note', type:'STRING'},
        {'name': 'sfa_code', type:'STRING'},
        {'name': 'farm_pk', type:'INTEGER'},
        {'name': 'vendor_pk', type:'INTEGER'},
        {'name': 'invoice_dt', type:'DATE'},
        {'name': 'program', type:'STRING'},
        {'name': 'customer_address_pk', type:'INTEGER'},
        {'name': 'reporting_product_pk', type:'INTEGER'},
        {'name': 'distance', type:'STRING'},
        {'name': 'import_id', type:'INTEGER'},
        {'name': 'import_purchase_pk', type:'INTEGER'},
        {'name': 'inverted_order_id', type:'INTEGER'},
        {'name': 'product_category_name', type:'STRING'},
        {'name': 'product_name', type:'STRING'},
        {'name': 'product_super_category_name', type:'STRING'},
        {'name': 'product_sub_category_name', type:'STRING'}
        ]},
      source_format='csv',
      skip_leading_rows=0,
      allow_jagged_rows='true',
      allow_quoted_newlines='true',
      write_disposition='WRITE_TRUNCATE')


#customer_addresses
    bq_raw_load_gcs_customer_addresses = gcs_to_bq.GoogleCloudStorageToBigQueryOperator(
    task_id='bq_raw_load_gcs_customer_addresses',
    bucket='aramark-datalake',
    destination_project_dataset_table='my-project-1495739920026:FarmLogixRaw.customer_addresses',
    source_objects=[
            'customer_addresses.csv'
        ],
    schema_fields= {'fields':
      [{'name': 'active_dt', type: 'DATETIME'},
      {'name': 'address_type', type: 'STRING'},
      {'name': 'city', type: 'STRING'},
      {'name': 'customer_pk',type:'INTEGER'},
      {'name': 'inactive_dt', type: 'DATETIME'},
      {'name': 'pk', type:'INTEGER'},
      {'name': 'state', type: 'STRING'},
      {'name': 'street_one', type: 'STRING'},
      {'name': 'street_two', type: 'STRING'},
      {'name': 'zip', type: 'STRING'},
      {'name': 'latitude', type: 'STRING'},
      {'name': 'longitude', type: 'STRING'},
      {'name': 'pr_cntr', type: 'STRING'},
      {'name': 'import_id', type: 'INTEGER'}
      ]},
      source_format='csv',
      skip_leading_rows=0,
      allow_jagged_rows='true',
      allow_quoted_newlines='true',
      write_disposition='WRITE_TRUNCATE')
#orders

    bq_raw_load_gcs_orders = gcs_to_bq.GoogleCloudStorageToBigQueryOperator(
    task_id='bq_raw_load_gcs_orders',
    bucket='aramark-datalake',
    destination_project_dataset_table='my-project-1495739920026:FarmLogixRaw.orders',
    source_objects=[
            'orders.csv'
        ],
    schema_fields= {'fields':
      [{'name': 'pk', type: 'INTEGER'},
      {'name': 'order_guide_pk', type: 'INTEGER'},
      {'name': 'customer_pk', type: 'INTEGER'},
      {'name': 'active_dt',type:'DATETIME'},
      {'name': 'inactive_dt', type: 'DATETIME'},
      {'name': 'request_dt', type:'DATETIME'},
      {'name': 'note', type: 'STRING'},
      {'name': 'distributor_pk', type: 'INTEGER'},
      {'name': 'parent_pk', type: 'INTEGER'},
      {'name': 'status', type: 'STRING'},
      {'name': 'user_pk', type: 'INTEGER'},
      {'name': 'organization_pk', type: 'INTEGER'},
      {'name': 'submission_dt', type: 'DATETIME'},
      {'name': 'fulfillment_dt', type: 'DATETIME'},
      {'name': 'review_dt', type: 'DATETIME'},
      {'name': 'cancel_dt', type: 'DATETIME'},
      {'name': 'confirmed_dt', type: 'DATETIME'},
      {'name': 'billing_address_city', type: 'STRING'},
      {'name': 'billing_address_state', type: 'STRING'},
      {'name': 'billing_address_street_one', type: 'STRING'},
      {'name': 'billing_address_street_two', type: 'STRING'},
      {'name': 'billing_address_zip', type: 'STRING'},
      {'name': 'billing_info_name', type: 'STRING'},
      {'name': 'shipping_info_name', type: 'STRING'},
      {'name': 'shipping_address_city', type: 'STRING'},
      {'name': 'shipping_address_state', type: 'STRING'},
      {'name': 'shipping_address_street_one', type: 'STRING'},
      {'name': 'shipping_address_street_two', type: 'STRING'},
      {'name': 'shipping_address_zip', type: 'STRING'},
      {'name': 'billing_info_contact1', type: 'STRING'},
      {'name': 'billing_info_contact2', type: 'STRING'},
      {'name': 'shipping_info_contact1', type: 'STRING'},
      {'name': 'shipping_info_contact2', type: 'STRING'},
      {'name': 'order_type', type: 'STRING'},
      {'name': 'order_id', type: 'INTEGER'},
      {'name': 'request_date_type', type: 'STRING'},
      {'name': 'request_end_dt', type: 'DATE'},
      {'name': 'created_by', type: 'INTEGER'},
      {'name': 'customer_order_nbr', type: 'STRING'},
      {'name': 'cp_invoice_nbr', type: 'STRING'},
      {'name': 'is_temporary', type: 'BOOLEAN'},
      {'name': 'pickup_location_pk', type: 'INTEGER'},
      {'name': 'source_pk', type: 'INTEGER'},
      {'name': 'customer_code', type: 'STRING'},
      {'name': 'import_id', type: 'STRING'}
      ]},
      source_format='csv',
      skip_leading_rows=0,
      allow_jagged_rows='true',
      allow_quoted_newlines='true',
      write_disposition='WRITE_TRUNCATE')

#products

    bq_raw_load_gcs_products = gcs_to_bq.GoogleCloudStorageToBigQueryOperator(
    task_id='bq_raw_load_gcs_products',
    bucket='aramark-datalake',
    destination_project_dataset_table='my-project-1495739920026:FarmLogixRaw.products',
    source_objects=[
            'products.csv'
        ],
    schema_fields= {'fields':
      [{'name': 'pk', type: 'INTEGER'},
      {'name': 'name', type: 'STRING'},
      {'name': 'code', type: 'STRING'},
      {'name': 'is_test',type:'INTEGER'},
      {'name': 'active_dt', type: 'DATETIME'},
      {'name': 'inactive_dt', type:'DATETIME'},
      {'name': 'parent_pk', type: 'INTEGER'},
      {'name': 'organization_pk', type: 'INTEGER'},
      {'name': 'ancestor_ids', type: 'STRING'},
      {'name': 'ancestor_names', type: 'STRING'},
      {'name': 'created_by', type: 'INTEGER'},
      {'name': 'photo_id', type: 'INTEGER'},
      {'name': 'description', type: 'STRING'},
      {'name': 'import_id', type: 'INTEGER'}
      ]},
      source_format='csv',
      skip_leading_rows=0,
      allow_jagged_rows='true',
      allow_quoted_newlines='true',
      write_disposition='WRITE_TRUNCATE')

#product_attribute
    bq_raw_load_gcs_product_attribute = gcs_to_bq.GoogleCloudStorageToBigQueryOperator(
    task_id='bq_raw_load_gcs_product_attribute',
    bucket='aramark-datalake',
    destination_project_dataset_table='my-project-1495739920026:FarmLogixRaw.product_attribute',
    source_objects=[
            'product_attribute.csv'
        ],
    schema_fields= {'fields':
      [{'name': 'pk', type: 'INTEGER'},
      {'name': 'category_id', type: 'INTEGER'},
      {'name': 'name', type: 'STRING'},
      {'name': 'created_by',type:'INTEGER'},
      {'name': 'active_dt', type: 'DATETIME'},
      {'name': 'import_id', type:'INTEGER'},
      {'name': 'long_name', type: 'STRING'},
      {'name': 'reporting_category_id', type: 'INTEGER'},
      {'name': 'code', type: 'STRING'}
      ]},
      source_format='csv',
      skip_leading_rows=0,
      allow_jagged_rows='true',
      allow_quoted_newlines='true',
      write_disposition='WRITE_TRUNCATE')

#product_attribute_category

    bq_raw_load_gcs_product_attribute_category = gcs_to_bq.GoogleCloudStorageToBigQueryOperator(
    task_id='bq_raw_load_gcs_product_attribute_category',
    bucket='aramark-datalake',
    destination_project_dataset_table='my-project-1495739920026:FarmLogixRaw.product_attribute_category',
    source_objects=[
            'product_attribute_category.csv'
        ],
    schema_fields= {'fields':
      [{'name': 'pk', type: 'INTEGER'},
      {'name': 'name', type: 'STRING'}
      ]},
      source_format='csv',
      skip_leading_rows=0,
      allow_jagged_rows='true',
      allow_quoted_newlines='true',
      write_disposition='WRITE_TRUNCATE')

#reporting_products_attributes

    bq_raw_load_gcs_reporting_products_attributes = gcs_to_bq.GoogleCloudStorageToBigQueryOperator(
    task_id='bq_raw_load_gcs_reporting_products_attributes',
    bucket='aramark-datalake',
    destination_project_dataset_table='my-project-1495739920026:FarmLogixRaw.reporting_products_attributes',
    source_objects=[
            'reporting_products_attributes.csv'
        ],
    schema_fields= {'fields':
      [{'name': 'reporting_product_id', type: 'INTEGER'},
      {'name': 'attribute_id', type: 'INTEGER'}
      ]},
      source_format='csv',
      skip_leading_rows=0,
      allow_jagged_rows='true',
      allow_quoted_newlines='true',
      write_disposition='WRITE_TRUNCATE')

#reporting_products

    bq_raw_load_gcs_reporting_products = gcs_to_bq.GoogleCloudStorageToBigQueryOperator(
    task_id='bq_raw_load_gcs_reporting_products',
    bucket='aramark-datalake',
    destination_project_dataset_table='my-project-1495739920026:FarmLogixRaw.reporting_products',
    source_objects=[
            'reporting_products.csv'
        ],
    schema_fields= {'fields':
      [{'name': 'pk', type: 'INTEGER'},
      {'name': 'armk_nbr', type: 'STRING'},
      {'name': 'case_weight', type: 'FLOAT'},
      {'name': 'created_by',type:'INTEGER'},
      {'name': 'active_dt', type: 'DATETIME'},
      {'name': 'distrib_nbr', type:'STRING'},
      {'name': 'farm_pk', type: 'INTEGER'},
      {'name': 'inactive_dt', type: 'DATETIME'},
      {'name': 'item', type: 'FLOAT'},
      {'name': 'mfg_nbr', type: 'STRING'},
      {'name': 'name', type: 'STRING'},
      {'name': 'organization_pk', type: 'INTEGER'},
      {'name': 'pack', type: 'FLOAT'},
      {'name': 'parent_pk', type: 'INTEGER'},
      {'name': 'sold_by', type: 'STRING'},
      {'name': 'uom', type: 'STRING'},
      {'name': 'sustainable', type: 'BOOLEAN'},
      {'name': 'local_by_dist_def', type: 'BOOLEAN'},
      {'name': 'import_id', type: 'INTEGER'}
      ]},
      source_format='csv',
      skip_leading_rows=0,
      allow_jagged_rows='true',
      allow_quoted_newlines='true',
      write_disposition='WRITE_TRUNCATE')

# Load Raw Layer - Farms
#    bq_raw_load_farms = bigquery_operator.BigQueryOperator(
 #     task_id='bq_raw_load_farms',
  #    bql="""      SELECT FarmPK, Brand, VendorCity
   #   FROM [my-project-1495739920026:aramarkUAT.farms]
    #  """,
     # destination_dataset_table=bq_dest_table, write_disposition='WRITE_TRUNCATE')

    # Define the order in which the tasks complete by using the >> and <<
    # operators.  
    #bq_raw_load_gcs_business_type >> 
    bq_raw_load_gcs_customers >> bq_raw_load_gcs_distributor_addresses >> bq_raw_load_gcs_distributors >> bq_raw_load_gcs_farm_addresses >> bq_raw_load_gcs_farms >> bq_raw_load_gcs_order_items >> bq_raw_load_gcs_customer_addresses >> bq_raw_load_gcs_orders >> bq_raw_load_gcs_products >> bq_raw_load_gcs_product_attribute >> bq_raw_load_gcs_product_attribute_category >> bq_raw_load_gcs_reporting_products_attributes >> bq_raw_load_gcs_reporting_products