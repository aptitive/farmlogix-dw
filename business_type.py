from __future__ import print_function

import datetime
import csv

from airflow import models
from airflow.operators import bash_operator
from airflow.operators import python_operator
from airflow.contrib.operators import bigquery_operator
from airflow.contrib.operators import gcs_to_bq
from airflow.contrib.operators.gcs_to_bq import GoogleCloudStorageToBigQueryOperator

#bq_dest_table = 'my-project-1495739920026:aramarkUAT.airflow_business_type'

default_dag_args = {
    # The start_date describes when a DAG is valid / can be run. Set this to a
    # fixed point in time rather than dynamically, since it is evaluated every
    # time a DAG is parsed. See:
    # https://airflow.apache.org/faq.html#what-s-the-deal-with-start-date
    'start_date': datetime.datetime(2018, 1, 1),
    'owner': 'farmlogix',
    'depends_on_past': False,
    'email': ['fred.bliss@aptitive.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 0,
    'schedule_interval': '@monthly',    
}

# Define a DAG (directed acyclic graph) of tasks.
# Any task you create within the context manager is automatically added to the
# DAG object.
with models.DAG(
        'bqtest-bt',
        default_args=default_dag_args) as dag:

# Load Raw Layer - Farms
    bq_raw_load_gcs_business_type = gcs_to_bq.GoogleCloudStorageToBigQueryOperator(
    task_id='bq_raw_load_gcs_business_type',
    bucket='aramark-datalake',
    destination_project_dataset_table='my-project-1495739920026:aramarkUAT.airflow_business_type',
    source_objects=[
            'business_type.csv'
        ],
    schema_fields= 
      [{"name":"int64_field_0", "type":"INTEGER"},
      {"name": "int64_field_1", "type":"INTEGER"},
      {"name": "timestamp_field_2", "type":"TIMESTAMP"},
	  {"name": "string_field_3", "type":"STRING"},
      {"name": "string_field_4", "type":"STRING"},
      {"name": "int64_field_5", "type":"INTEGER"},
      {"name": "int64_field_6", "type":"INTEGER"}],
    source_format='csv',
    field_delimiter=',',
    skip_leading_rows=0,
    max_bad_records=5,
    allow_jagged_rows='true',
    allow_quoted_newlines='true',
    write_disposition='WRITE_TRUNCATE',
    create_disposition='CREATE_IF_NEEDED')    

# Example of loading from Raw to Business in BQ
#    bq_raw_load_farms = bigquery_operator.BigQueryOperator(
 #     task_id='bq_raw_load_farms',
  #    bql="""      SELECT FarmPK, Brand, VendorCity
   #   FROM [my-project-1495739920026:aramarkUAT.farms]
    #  """,
     # destination_dataset_table=bq_dest_table, write_disposition='WRITE_TRUNCATE')

    # Define the order in which the tasks complete by using the >> and <<
    # operators. 
    bq_raw_load_gcs_business_type